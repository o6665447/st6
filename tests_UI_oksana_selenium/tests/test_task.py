import pytest


@pytest.mark.smoke
def test_registration_with_invalid_email(create_account):
    create_account.open()
    first_name = "John"
    last_name = "Smith"
    invalid_email = "not_an_email"
    password = "John2024!"
    create_account.complete_registration(first_name, last_name, invalid_email, password)
    create_account.verify_email_error_message('Please enter a valid email address')


@pytest.mark.regression
def test_successful_registration_redirection(create_account):
    create_account.open()
    new_email = create_account.generate_email()
    first_name = "John"
    last_name = "Smith"
    password = "John2024!"
    expected_redirection_url = "https://magento.softwaretestingboard.com/customer/account/"
    create_account.complete_registration(first_name, last_name, new_email, password)
    create_account.verify_redirection(expected_redirection_url)


def test_search_field(sale_page):
    sale_page.open()
    sale_page.search_for_product("123")
    sale_page.verify_search_results('Your search returned no results.')


def test_home_page_load(sale_page):
    sale_page.open()
    sale_page.click_more_button()
    sale_page.double_click_second_product()
    sale_page.verify_product_availability("IN STOCK")


def test_elements_presence(sale_page):
    sale_page.open()
    sale_page.navigate_to_shorts()
    sale_page.verify_product_images()
    sale_page.verify_add_to_cart_buttons()
